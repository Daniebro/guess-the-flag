//
//  ViewController.swift
//  Project2
//
//  Created by Danni André on 9/7/19.
//  Copyright © 2019 Danni André. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    
    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    var questionsAsked = 0
    var highScore: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        registerLocal()
        
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
        
        button1.layer.borderWidth = 1
        button2.layer.borderWidth = 1
        button3.layer.borderWidth = 1
        
        button1.layer.borderColor = UIColor.lightGray.cgColor
        button2.layer.borderColor = UIColor.lightGray.cgColor
        button3.layer.borderColor = UIColor.lightGray.cgColor
        
        askQuestion(action: nil)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(showScore))
        
        let defaults = UserDefaults.standard
        highScore = defaults.integer(forKey: "highScore")
        
    }
    
    func askQuestion(action: UIAlertAction!) {
        button1.transform = .identity
        button2.transform = .identity
        button3.transform = .identity
        
        countries.shuffle()
        
        correctAnswer = Int.random(in: 0...2)
        
        button1.setImage(UIImage(named: countries[0]), for: .normal)
        button2.setImage(UIImage(named: countries[1]), for: .normal)
        button3.setImage(UIImage(named: countries[2]), for: .normal)
        
        title = countries[correctAnswer].uppercased() + " Score: \(score)"
        
    }

    @IBAction func buttonTapped(_ sender: UIButton) {
        var title: String
        
        
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: [], animations: {
            sender.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        }) { (finished) in
            
        }
        
        if sender.tag == correctAnswer {
            title = "Correct"
            score += 1
        } else {
            title = "Wrong, thats the flag of \(countries[sender.tag].uppercased())"
            score -= 1
        }
        questionsAsked += 1
        print(questionsAsked)
        print(highScore)
        let ac: UIAlertController
        
        if questionsAsked == 5 {
            if score > highScore {
                highScore = score
                ac = UIAlertController(title: title, message: "NEW HIGH SCORE is \(score)", preferredStyle: .alert)
                save()
            } else {
                ac = UIAlertController(title: title, message: "Game over! Your score is \(score)", preferredStyle: .alert)
            }
            score = 0
            questionsAsked = 0
            
        } else {
            ac = UIAlertController(title: title, message: "Your score is \(score)", preferredStyle: .alert)
        }
        ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestion))
        
        present(ac, animated: true)
        
        
        
        
    }
    
    @objc func showScore(){
        let ad = UIAlertController(title: "Score", message: "Your score is: \(score)", preferredStyle: .alert)
        ad.addAction(UIAlertAction(title: "continue", style: .default, handler: nil))
        present(ad, animated: true)
        
        
    }
    func save(){
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(highScore){
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "highScore")
        } else {
            print("failed to save highScore")
        }
    }
    
    func scheduleNotifications() {
        registerCategories()
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        
        let content = UNMutableNotificationContent()
        content.title = "Time to learn"
        content.body = "Don't forget to do today's Unwrap challenge"
        content.categoryIdentifier = "alarm"
        content.sound = .default
        
        let secondsInDay = 86400.0
//        let secondsInDay = 5.0
        
        for i in 1...7 {
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: Double(i) * secondsInDay, repeats: false )
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            center.add(request)
        }
        
    }
    func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self

        let show = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
        let category = UNNotificationCategory(identifier: "alarm", actions: [show], intentIdentifiers: [])

        center.setNotificationCategories([category])
    }
    
    
    
    
    func registerLocal(){
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { [weak self] (granted, error) in
            if granted {
                print("yay!")
                self?.scheduleNotifications()
            } else {
                print("D'oh!")
            }
        }
    }
    
    
    

}

